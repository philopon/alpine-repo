# alpine-repo
my alpine linux repository

# How to use
```console
# echo https://philopon.gitlab.io/alpine-repo >> /etc/apk/repositories
# wget -P /etc/apk/keys https://philopon.gitlab.io/alpine-repo/philopon.dependence@gmail.com-5add7efe.rsa.pub
# apk add --update py3-rdkit
```
